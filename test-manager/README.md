## Prerequisites

To enable user authentication in React application you have to run docker container with Keycloak service https://gitlab.com/7bc-ds/h-alo/halo-backoffice

1. use `git clone https://gitlab.com/7bc-ds/h-alo/halo-backoffice` to download the repository.
2. to run server you have to use docker. You can download it here - https://docs.docker.com/engine/install/ubuntu/
3. `docker-compose up` inside the project folder `/halo-backoffice/ to start server.
4. In case of: `ERROR: Version in "./docker-compose.yml" is unsupported.` your docker version isn't the newest one so you need to update it or change 
version in first line in docker-compose.yml file, from 3.9 to 3.7. 
5. You can Log In to admin site of Keycloak http://127.0.0.1:8180/auth/admin login="admin", password="admin"
6. Later, using React app after redirection Log In as user1: login="user1", password="user1"

## Run React Application
Created with:
- node v16.13.2
- npm v8.1.2

In the project directory, you can run:

1. `npm i` to install packages
2. `npm run` to start server

### Temporary json-server with mocked data
To have all data necessary to display organizations and their sites there is created json-server instance with mocked data stored in db.json file. 

To run json server:
1. inside directory /halo-web/ run: `json-server --watch db.json --port 3001`
 
## Run dockerized app
To run app in docker container one can simply:
1. use 'docker-compose up'
OR
2. use './buildWebapp.sh' to build Docker image and then run it with: 'docker run -itd --rm -v ${PWD}:/app -v /app/node_modules -p 3001:3000 -e CHOKIDAR_USEPOLLING=true halo-webapp'
