import React from "react";

export interface SiteStats {
  avgScore: number;
  minScore: number;
  maxScore: number;
  testsCount: number;
}

export interface Sites {
  name: string;
  siteId: string;
  siteStats: SiteStats;
  trend: string;
  lastStatus: string;
  lastResult: Date;

  map(element: (el: Sites) => JSX.Element): React.ReactNode | undefined;
}

export type Organization = {
  id: number;
  name: string;
  sites: Sites;
};

export interface Measurement {
  resultId: number;
  resultTS: string;
  contributor: string;
  device: string;
  cartridge: string;
  batch: string;
  result: string;
  comment: string;
  tags: string[];
  status: string;
}

export type OrganizationParams = {
  name: string;
};


export type Filter = {
  batch: string[];
  device: string[];
  user: string[];
};

export type ErrorLevel = {
  contaminant: string;
  warningLevel?: number | string;
  errorLevel?: number | string;
  unit?: string;
  errorId?: string;
};
