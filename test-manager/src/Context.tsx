import { createContext } from "react";

export type KeycloakState = {
  keycloak: object;
  keycloakToken: string | undefined;
  authenticated: boolean;
  username: string;
  isAdmin: boolean;
  organizationId: string;
}

export const initialState: KeycloakState = {
  keycloak: {},
  keycloakToken: "",
  authenticated: false,
  username: "",
  isAdmin: false,
  organizationId: "",
}

export function instanceOfKeycloakState(object: any): object is KeycloakState {
  if (Object.keys(object.keycloak).length !== 0) {
    return true;
  }
  return false;
}

export const KeycloakContext = createContext({
    state: initialState,
    setState: (ctx: KeycloakState) => {}
  });
