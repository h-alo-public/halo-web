import React from "react";
import SiteTable from "../components/SiteTable/SiteTable";

const Site: React.FC = () => {
  return (
    <div className="page-container">
      <SiteTable />
    </div>
  );
};

export default Site;
