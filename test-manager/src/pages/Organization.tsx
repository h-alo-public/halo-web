import React from "react";

import OrganizationSelect from "../components/OrganizationSelect/OrganizationSelect";

const Organization: React.FC = (props: any) => {
  return <OrganizationSelect organizationName={props.name} />;
};

export default Organization;
