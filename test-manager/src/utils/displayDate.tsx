export const displayDate = (dateProps: Date) => {
  let date = new Date(dateProps);
  return `${date.getFullYear()}.${("0" + (date.getMonth() + 1)).slice(-2)}.${("0" + date.getDate()).slice(-2)}`;
};
