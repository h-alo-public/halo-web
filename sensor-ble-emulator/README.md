# H'alo Sensor BLE Emulator (HSBLEE)

This is Android Studio project with application emulating BLE Device with:
- Service UUID: 69910853-d07a-41f7-aa1d-356f59331e00
- Characteristic UUID: 6edfc2c2-aa44-4cd3-8dbf-f835d7ca8ede
- Allowing notifications
- Value type: 32 byte int

One can install/run it by:
- Uploading from Android Studio
or
- Installing ./app/release/app-release.apk on Android device (version at least 9.0) - NOTE: not all devices may support BLE technology.

