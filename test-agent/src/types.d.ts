import React from "react";

export type ResultToSend = {
  resultTS: string | undefined;
  device: string | undefined;
  cartridge: string | undefined;
  result: string | undefined;
  batch: string | undefined;
  contributor: string | undefined;
  status: string | undefined;
  comment: string | undefined;
  tags: string[] | undefined;
};

export interface Sites {
  name: string;
  siteId: string;
  siteStats: SiteStats;
  trend: string;
  lastStatus: string;
  lastResult: Date;

  map(element: (el: Sites) => JSX.Element): React.ReactNode | undefined;
}

export interface Measurement {
  resultId: number;
  resultTS: string;
  contributor: string;
  device: string;
  cartridge: string;
  batch: string;
  result: string;
  comment: string;
  tags: string[];
  status: string;
}
