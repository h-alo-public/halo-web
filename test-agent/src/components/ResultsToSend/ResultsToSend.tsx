import React, { useEffect, useState } from "react";
import "./style.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileLines } from "@fortawesome/free-solid-svg-icons";
import { Col, Container, Row } from "react-bootstrap";
import { get } from "idb-keyval";

const ResultsToSend: React.FC = () => {
  const [resultsCount, setResultsCount] = useState<number | undefined>(
    undefined
  );
  useEffect(() => {
    get("results")
      .then((resp) => setResultsCount(resp.length))
      .catch((error) => console.error(error));
  }, []);

  return (
    <div className="results-to-send">
      <Container>
        <Row>
          <Col xs={2}>
            <FontAwesomeIcon className="icon" icon={faFileLines} size="lg" />
          </Col>
          <Col xs={10}>
            <div className="text">
              {resultsCount ? (
                <>
                  You have{" "}
                  <span style={{ fontWeight: "bold" }}>{resultsCount}</span>{" "}
                  test result{resultsCount !== 1 && "s"} to send.
                </>
              ) : (
                "You don't have any test yet to send."
              )}
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ResultsToSend;
