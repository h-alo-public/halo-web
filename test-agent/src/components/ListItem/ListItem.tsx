import React, { useCallback, useEffect, useState } from "react";
import "./style.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTag } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import { faCommentDots, faTrashCan } from "@fortawesome/free-regular-svg-icons";
import { get, set } from "idb-keyval";
import { ResultToSend } from "../../types";
import ConfirmDeleteModal from "../../modals/ConfirmDeleteModal/ConfirmDeleteModal";
import { useRecoilState } from "recoil";
import { resultsListAtom } from "../../recoil/atoms";
import TestDetailsModal from "../../modals/TestDetailsModal/TestDetailsModal";

interface Props {
  id: number;
  date: string | undefined;
  device: string | undefined;
  value?: string | undefined;
  onRemoveElement: (results: ResultToSend[]) => void;
}

interface Modals {
  name: "ConfirmDelete" | "TestDetails";
}

type VisibleModal = Modals | undefined;

const ListItem: React.FC<Props> = ({
  id,
  date,
  device,
  value,
  onRemoveElement,
}) => {
  const [visibleModal, setVisibleModal] = useState<VisibleModal>();
  const [resultsList, setResultsList] = useState([]);
  const [, setResults] = useRecoilState(resultsListAtom);

  useEffect(() => {
    get("results").then((res) => setResultsList(res));
  }, []);

  const handleRemoveListItem = useCallback(
    (id: number) => {
      const updatedResultsList = resultsList;
      updatedResultsList.splice(id, 1);
      set("results", updatedResultsList).then(() => {
        setResultsList(updatedResultsList);
        setResults(updatedResultsList);
      });

      onRemoveElement(updatedResultsList);
    },
    [onRemoveElement, resultsList, setResults]
  );

  const handleModalClose = useCallback(() => {
    setVisibleModal(undefined);
  }, []);

  const handleModalOpen = useCallback((modalType: Modals) => {
    setVisibleModal({ name: modalType.name });
  }, []);

  const renderModals = useCallback(() => {
    if (visibleModal !== undefined) {
      switch (visibleModal.name) {
        case "ConfirmDelete":
          return (
            <ConfirmDeleteModal
              onClose={handleModalClose}
              handleRemoveListItem={handleRemoveListItem}
              elementId={id}
            />
          );
        case "TestDetails":
          return (
            <TestDetailsModal
              date={date}
              device={device}
              elementId={id}
              onClose={handleModalClose}
            />
          );
      }
    }
  }, [date, device, handleModalClose, handleRemoveListItem, id, visibleModal]);

  return (
    <div className="list-item">
      <Row>
        <Col
          xs={10}
          className="description"
          onClick={() => handleModalOpen({ name: "TestDetails" })}
          style={{ overflowWrap: "break-word", maxWidth: "100%" }}
        >
          <Row>{date}</Row>
          <Row className="d-flex justify-content-start">
            <div className="deviceContainer">
              <div style={{ minWidth: "60px", paddingLeft: 0 }}>Device:</div>{" "}
              <span style={{ textAlign: "start" }}>{device}</span>
            </div>
          </Row>
          <Row className="icons">
            <FontAwesomeIcon className="tag" icon={faTag} />
            <FontAwesomeIcon className="comment" icon={faCommentDots} />
          </Row>
        </Col>
        <Col xs={2} className="icon delete-icon">
          <FontAwesomeIcon
            onClick={() => handleModalOpen({ name: "ConfirmDelete" })}
            icon={faTrashCan}
          />
        </Col>
      </Row>
      {renderModals()}
    </div>
  );
};

export default ListItem;
