import * as React from "react";
import { useCallback } from "react";
import { Navigate } from "react-router-dom";

import { useKeycloak } from "@react-keycloak/web";

const Login = () => {
  const { keycloak } = useKeycloak();

  const login = useCallback(() => {
    keycloak?.login();
  }, [keycloak]);

  if (keycloak?.authenticated)
    return <Navigate replace to={`/organization/:name/start-page`} />;

  return null;
};

export default Login;
