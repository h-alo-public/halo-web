import React, { useState } from "react";
import "./style.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBan } from "@fortawesome/free-solid-svg-icons";
import { Button, Col, Container, Row } from "react-bootstrap";
import { useRecoilState } from "recoil";
import {
  measurementDeviceAtom,
  deviceCharacteristicAtom,
} from "../../recoil/atoms";

const ConnectedDevice: React.FC = () => {
  ///////////////////////////////////////
  // Works in Chrome with following chrome://flags/ enabled:
  // #enable-experimental-web-platform-features
  // #enable-web-bluetooth-new-permissions-backend
  // And one for self-signed SSL:
  // #allow-insecure-localhost
  ///////////////////////////////////////

  const [notifier, setNotifier] = useState(false);
  const [measurementDevice, setMeasurementDevice] = useRecoilState(
    measurementDeviceAtom
  );
  const [deviceCharacteristic, setDeviceCharacteristic] = useRecoilState(
    deviceCharacteristicAtom
  );

  async function connectDevice() {
    navigator.bluetooth
      .requestDevice({
        filters: [{ services: ["69910853-d07a-41f7-aa1d-356f59331e00"] }],
      })
      .then((dev) => {
        console.log("Device found: ", dev);
        setMeasurementDevice((measurementDevice) => ({
          ...measurementDevice,
          isMeasurementDeviceConnected: true,
          device: dev,
        }));
        return dev.gatt?.connect();
      })
      .then((server) => {
        console.log("Server connected: ", server);
        return server?.getPrimaryService(
          "69910853-d07a-41f7-aa1d-356f59331e00"
        );
      })
      .then((service) => {
        console.log("Primary service: ", service);
        return service?.getCharacteristic(
          "6edfc2c2-aa44-4cd3-8dbf-f835d7ca8ede"
        );
      })
      .then((chrctr) => {
        setDeviceCharacteristic({
          characteristic: chrctr || null,
        });
        console.log("Characteristic: ", chrctr);
        chrctr?.addEventListener("characteristicvaluechanged", (event) => {
          if (!event.target) return;
          // @ts-ignore
          const value = event.target.value;
          const timestampSection = new Uint8Array(value.buffer).slice(4, 20);

          const timeEncoded = [];
          for (let i = 0; i < timestampSection.length; i++) {
            timeEncoded.push(String.fromCharCode(timestampSection[i]));
          }
        });
      })
      .catch((error) => {
        console.log(error);
        setMeasurementDevice((measurementDevice) => ({
          ...measurementDevice,
          isMeasurementDeviceConnected: false,
        }));
      });
  }

  // async function signUp() {
  //   if (deviceCharacteristic.characteristic) {
  //     try {
  //       await deviceCharacteristic.characteristic.startNotifications();
  //       console.log("Notifications started");
  //       setNotifier(true);
  //     } catch (error) {
  //       console.log("Starting notifications failed: " + error);
  //     }
  //   } else {
  //     console.log("No characteristic found yet");
  //   }
  // }

  async function signOut() {
    if (deviceCharacteristic.characteristic) {
      try {
        await deviceCharacteristic.characteristic.stopNotifications();
        console.log("Notifications stopped");
        setNotifier(false);
      } catch (error) {
        console.log("Stopping notifications failed: " + error);
      }
    } else {
      console.log("No characteristic found yet");
    }
  }

  return (
    <div className="connected-device">
      {measurementDevice.isMeasurementDeviceConnected ? (
        <Container>
          <Row>
            <Col>
              <span>
                You are connected to:{" "}
                <span style={{ color: "black" }}>
                  (
                  {measurementDevice.device?.name ||
                    measurementDevice.device?.id}
                  )
                </span>
              </span>
            </Col>
          </Row>
          {deviceCharacteristic.characteristic && (
            <Row>
              <Col>
                {!notifier && (
                  <span>
                    <Button
                      className="button-connect-device"
                      onClick={connectDevice}
                    >
                      Connect Another Device
                    </Button>
                  </span>
                )}
                {notifier && (
                  <Button className="button-connect-device" onClick={signOut}>
                    Sign out from notifications
                  </Button>
                )}
              </Col>
            </Row>
          )}
        </Container>
      ) : (
        <Container>
          <Row>
            <Col xs={2}>
              <FontAwesomeIcon className="icon" icon={faBan} size="lg" />
            </Col>
            <Col xs={10}>
              <div className="text">You are not connected to any device</div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="button">
                <Button
                  className="button-connect-device"
                  onClick={connectDevice}
                >
                  Scan for devices
                </Button>
              </div>
            </Col>
          </Row>
        </Container>
      )}
    </div>
  );
};

export default ConnectedDevice;
