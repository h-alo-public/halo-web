import { get, set } from "idb-keyval";
import React, { useEffect, useState } from "react";
import "./style.scss";
import { ListGroup } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useRecoilState } from "recoil";
import { securedApi } from "../../api";
import { resultsListAtom } from "../../recoil/atoms";
import { ResultToSend } from "../../types";
import ListItem from "../ListItem/ListItem";
import UploadResultsItem from "../UploadResultsItem/UploadResultsItem";

const API_URL: string | undefined = process.env.REACT_APP_API;

interface Props {
  setIsUploading: (val: boolean) => void;
}
const ResultsList: React.FC<Props> = ({ setIsUploading }) => {
  const [siteInfo, setSiteInfo] =
    useState<{ siteId: string; siteName: string }>();
  const [resultsToDisplay, setResultsToDisplay] = useState<
    ResultToSend[] | undefined
  >(undefined);
  const [results, setResults] = useRecoilState(resultsListAtom);
  const navigate = useNavigate();

  const fetchData = () => {
    get("results").then((res) => {
      setResultsToDisplay(res);
      setResults(res);
    });
    get("currentSite").then((site) => {
      setSiteInfo(site);
    });
  };

  const handleUpload = async (): Promise<void> => {
    setIsUploading(true);

    try {
      for (const result of results) {
        await securedApi.post(
          `${API_URL}/api/sites/${siteInfo?.siteId}/results`,
          result
        );
      }

      set("results", []).catch((error) => console.error(error));
      setResultsToDisplay([]);
      setResults([]);
      setTimeout(() => {
        setIsUploading(false);
        navigate("/success");
      }, 3000);

      fetchData();
    } catch (error) {
      console.error("Error uploading results:", error);
      navigate("/*");
    }
  };
  useEffect(() => {
    fetchData();
  }, [setResultsToDisplay]);

  return (
    <div className="results-list-container">
      <div className="list">
        {resultsToDisplay !== undefined && resultsToDisplay.length > 0 ? (
          <ListGroup>
            {resultsToDisplay.map(({ resultTS, device, result }, idx) => {
              return (
                <ListGroup.Item key={idx} className="item">
                  <ListItem
                    id={idx}
                    date={resultTS}
                    device={device}
                    value={result}
                    onRemoveElement={setResultsToDisplay}
                  />
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        ) : (
          <div>There are no results to upload</div>
        )}
      </div>
      {results && results.length > 0 && (
        <UploadResultsItem handleUpload={handleUpload} />
      )}
    </div>
  );
};

export default ResultsList;
