import { atom } from "recoil";
import { ResultToSend } from "../../types";

export const resultsListAtom = atom<ResultToSend[]>({
  key: "results",
  default: [],
});
