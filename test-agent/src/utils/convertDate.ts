export const convertDate = (el: string | undefined) => {
  return el && new Date(el).toISOString().slice(0, 19).replace("T", " ");
};
