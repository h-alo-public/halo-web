import React from "react";
import "../style-common.scss";
import "./style.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileImage } from "@fortawesome/free-regular-svg-icons";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { userInfoAtom } from "../../recoil/atoms";

const StartNewTest: React.FC = () => {
  const navigate = useNavigate();

  const userInfo = useRecoilValue(userInfoAtom);

  return (
    <div className="start-new-test">
      <div className="header">
        <h1>Start New Test</h1>
      </div>
      <div className="subheader">
        Put sample into your testing device and hit Start.
      </div>
      <div className="picture">
        <FontAwesomeIcon className="icon" icon={faFileImage} />
      </div>
      <div className="button">
        <Button
          className="button-go-test"
          onClick={() => {
            navigate(
              `/organization/${userInfo.currentOrganizationName}/test-in-progress`
            );
          }}
        >
          Start
        </Button>
      </div>
    </div>
  );
};

export default StartNewTest;
