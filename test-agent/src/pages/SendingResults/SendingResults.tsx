import React, { useEffect, useState } from "react";
import "../../App.scss";
import "./style.scss";
import "../style-common.scss";
import { faCloudArrowUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "react-bootstrap";
import { securedApi } from "../../api";
import { useRecoilValue } from "recoil";
import { currentSite } from "../../atoms";
import { useNavigate } from "react-router-dom";
import { get, set } from "idb-keyval";

const API_URL: string | undefined = process.env.REACT_APP_API;

const SendingResults: React.FC = () => {
  const navigate = useNavigate();
  const siteInfo = useRecoilValue(currentSite);
  const [resultsList, setResultsList] = useState([]);
  const [singleResult, setSingleResult] = useState();

  useEffect(() => {
    get("results").then((res) => {
      setSingleResult(res[res.length - 1]);
      setResultsList(res);
    });
  }, []);

  const handleUpload = (): void => {
    securedApi
      .post(`${API_URL}/api/sites/${siteInfo?.siteId}/results`, singleResult)
      .then(() => {
        const newResultsList = resultsList.slice(0, resultsList.length - 1);

        set("results", newResultsList).catch((error) => console.error(error));
        navigate(`/success`);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="sending-results">
      <h1 className="header">Sending last result...</h1>
      <div className="icon">
        <FontAwesomeIcon
          className="upload-icon"
          icon={faCloudArrowUp}
          size="sm"
        />
      </div>
      <div className="button">
        <Button className="button-cancel" onClick={handleUpload}>
          Complete
        </Button>
      </div>
    </div>
  );
};

export default SendingResults;
