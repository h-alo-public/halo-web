import { atom } from "recoil";

export const currentResult = atom({
    key: "currentResult",
    default: {
      resultId: "",
    },
  });

  export const currentSite = atom({
    key: "currentSite",
    default: {
      siteId: "",
      siteName: "",
    },
  });