const KEYCLOAK_URL = process.env.REACT_APP_KEYCLOAK_ADDRESS;

const config = {
  url: KEYCLOAK_URL,
  realm: "halo-keycloak",
  clientId: "springboot-keycloak",
};

export default config;
